# ANXCamera

## Getting Started :
### Cloning :
- Clone this repo in vendor/aeonax/ANXCamera in your working directory by :
```
git clone -b <branch-name> https://gitlab.com/madhavbiju/vendor_aeonax_anxcamera.git vendor/aeonax/ANXCamera 
```
### Changes Required :
- You will need [these changes in your device tree.](https://github.com/madhavbiju/S_device_xiaomi_violet/commit/75d23a83e01e10bf1793e09a9fc042882e23bba8)
- Done, continue building your ROM as you do normally.
